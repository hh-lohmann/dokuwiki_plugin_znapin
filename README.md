---
# dokuwiki_plugin_znapin

Copyright (C) hh.lohmann <hh.lohmann@yahoo.de>.
See LICENSE file in this package for details. 
Third party copyrights are indicated in code when appropriate.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

**DISCLAIMER**

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


## Purpose

The ZnapIn plugin allows for boilerplates which are meant as an alternative to plugins, but are maintained on "local" DokuWiki pages. With DokuWiki's *htmlok* and *phpok* enabled one comes near to the power of real plugins. Technically, its just a special call of the outstanding Include plugin which has to be installed independently.

Since *ZnapIns* are DokuWiki page content, one may keep "repositories" for them inside the DokuWiki where they are used, allowing for quick changes and adaption with common DokuWiki tools like versioning and with only local impact, i.e. other than "real" plugins an error in a ZnapIn can only corrupt the pages where it is used but not DokuWiki as a whole - kisses of death are beyond the scope of ZnapIns. This dramatically reduces the effort for testing changes or adapting functions.


##Installation

1. Install the Include plugin (https://www.dokuwiki.org/plugin:include) if not already installed
1. Install the ZnapIn plugin here via DokuWiki's plugin manager and the URL https://bitbucket.org/hh-lohmann/dokuwiki_plugin_znapin/get/master.zip


## Usage

1. Create a DokuWiki namespace ```:common:inc:``` to host your ZnapIns (in future versions, the namespace where ZnapIns reside is planned to be configurable)
1. Create ZnapIns = arbitrary DokuWiki pages with a section titled as "Code" - this section's content will be included as the ZnapIn, anything on the ZnapIn's page outside the "Code" section can be used for information, tracking, planning or anything concerning the ZnapIn (consider the data plugin for indexing your ZnapIns)
1. Include ZnapIns where you need them = put the line ```~~znapin:NAME-OF-THE-ZNAPIN-DOKUWIKI-PAGE~~``` in a page's content - it becomes obvious that it would be good practice to name the page containing the ZnapIn with the ZnapIn's "name" (or an abbreviation of it)


Full documentation and code is available under https://bitbucket.org/hh-lohmann/dokuwiki_plugin_znapin.